<?php

namespace TMCms\Modules\Icons;

use TMCms\Templates\PageHead;
use TMCms\Templates\PageTail;

class ModuleIcons
{
    static private $_icons = [];

    /**
     * @param $icons_file
     * @param string $prefix
     * @param bool|true $add_empty
     * @param bool|true $add_style
     * @param mixed|false $select2_prefix see prepareSelect2
     * @return mixed
     */
    public static function getPairs($icons_file, $prefix='', $add_empty = true, $add_style = true, $select2_prefix = false)
    {
        if($select2_prefix){
            self::prepareSelect2($prefix, $select2_prefix);
        }
        if($add_style) {
            PageHead::getInstance()->addCssUrl($icons_file);
            if(MODE=='cms'){
                PageHead::getInstance()
                    ->addCss('
                        .admin-icon[class^="'.$prefix.'-"], .admin-icon[class*=" '.$prefix.'-"] {
                            font-size: 22px;
                        }
                    ');
            }
        }

        if(!isset(self::$_icons[$prefix])){
            $path = DIR_BASE;
            if($add_empty)
                $data = array(''=>'');
            else
                $data = [];
            if(file_exists($path.$icons_file)) {
                $handle = fopen($path . $icons_file, "r");
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        if (preg_match('/.([a-zA-Z0-9-_]+):before {/', $line, $matches)) {
                            $v = $prefix ? str_replace($prefix.'-','',$matches[1]) : $matches[1];
                            $data[$v] = $v;
                        }
                    }
                    fclose($handle);
                } else {
                    // error opening the file.
                }
            }
            self::$_icons[$prefix] = $data;
        }

        return self::$_icons[$prefix];
    }

    /**
     * @param $icon_prefix
     * @param string|array $selectors contains select selector, for ex. select[id^=home_benefits_icon]
     */
    public static function prepareSelect2($icon_prefix, $selectors){
        PageTail::getInstance()->addJs('
                function formatIcon(item){
                    if (!item.id) return item.text; // optgroup
                    return "<i class=\"admin-icon '.$icon_prefix.'-"+item.id+"\"></i> "+item.text;
                }
                $(function(){
                    $("'.implode(',', (array)$selectors).'").select2("destroy").select2({
                        allowClear:true,
                        placeholder: "none",
                        formatResult: formatIcon,
                        formatSelection: formatIcon,
                        escapeMarkup: function(m) { return m; }
                    });
                })
            ');
    }

}